#![no_std]
#![warn(clippy::all, clippy::pedantic, clippy::nursery, clippy::cargo)]
#![allow(
    clippy::explicit_deref_methods,
    clippy::if_not_else,
    clippy::redundant_pub_crate
)]

#[cfg(feature = "sleipnir")]
use alloc::fmt::format;
use {
    crate::alloc::string::ToString,
    abi_stable::{std_types::RString, StableAbi},
    core::{
        fmt::{Display, Formatter},
        num::{NonZeroIsize, NonZeroUsize},
        ops::Range,
    },
    thiserror_no_std::Error as ThisError,
};

extern crate alloc;

#[repr(C)]
#[derive(Clone, Debug, Eq, PartialEq, StableAbi, ThisError)]
pub enum Error {
    #[error("state not recoverable: {0}")]
    StateNotRecoverable(RString),
    #[error("operation cancelled")]
    OperationCancelled,
    #[error("the channel counterparts shutdown")]
    ConnectionShutdown,
    #[error("not supported: {0}")]
    NotSupported(RString),
    #[error("numerical result out of range: [{0}, {1}) does not contain {2}")]
    ResultOutOfRange(IntegerWrapper, IntegerWrapper, IntegerWrapper),
    #[error("invalid argument: {0}")]
    InvalidArgument(RString),
    #[error("mathematics argument out of domain of function: [{0}, {1}) does not contain {2}")]
    ArgumentOutOfDomain(IntegerWrapper, IntegerWrapper, IntegerWrapper),
    #[error("no such file or directory")]
    NoSuchFileOrDirectory,
    #[error("operation not permitted: {0}")]
    OperationNotPermitted(RString),
    #[error("a host memory allocation has failed")]
    OutOfHostMemory,
    #[error("a device memory allocation has failed")]
    OutOfDeviceMemory,
    #[error(
        "initialization of an object could not be completed for implementation-specific reasons: {0}")]
    InitializationFailed(RString),
    #[error("the device has been lost.")]
    DeviceLost,
    #[error("mapping of a memory object has failed.")]
    MemoryMapFailed,
    #[error("a requested layer is not present or could not be loaded: {0}")]
    LayerNotPresent(RString),
    #[error("a requested extension is not supported: {0}")]
    ExtensionNotPresent(RString),
    #[error("a requested feature is not supported: {0}")]
    FeatureNotPresent(RString),
    #[error(
        "the requested version of Vulkan is not supported by the driver or is otherwise \
        incompatible for implementation-specific reasons"
    )]
    IncompatibleDriver,
    #[error("too many objects of the type have already been created")]
    TooManyObjects,
    #[error("one or more shaders failed to compile or link")]
    InvalidShader,
    #[error(
        "a buffer creation or memory allocation failed because the requested address is not \
        available or a shader group handle assignment failed because the requested shader group \
        handle information is no longer valid"
    )]
    InvalidOpaqueCaptureAddress,
    #[error("an image creation failed because internal resources required for compression are exhausted")]
    CompressionExhausted,
    #[error("unknown")]
    Unknown,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub enum IntegerWrapper {
    I8(i8),
    U8(u8),
    I16(i16),
    U16(u16),
    I32(i32),
    U32(u32),
    I64(i64),
    U64(u64),
    Isize(isize),
    Usize(usize),
    NonZeroIsize(NonZeroIsize),
    NonZeroUsize(NonZeroUsize),
}

#[cold]
#[inline]
#[must_use]
pub fn state_not_recoverable(reason: &str) -> Error {
    Error::StateNotRecoverable(reason.into())
}

#[cold]
#[inline]
#[must_use]
pub const fn operation_cancelled() -> Error {
    Error::OperationCancelled
}

#[cold]
#[inline]
#[must_use]
pub const fn connection_shutdown() -> Error {
    Error::ConnectionShutdown
}

#[cold]
#[inline]
#[must_use]
pub fn not_supported(reason: &str) -> Error {
    Error::NotSupported(reason.to_string().into())
}

#[cold]
#[inline]
#[must_use]
pub const fn result_out_of_range(range: Range<IntegerWrapper>, result: IntegerWrapper) -> Error {
    Error::ResultOutOfRange(range.start, range.end, result)
}

#[cold]
#[inline]
#[must_use]
pub fn invalid_argument(reason: &str) -> Error {
    Error::InvalidArgument(reason.to_string().into())
}

#[cold]
#[inline]
#[must_use]
pub const fn argument_out_of_domain(range: Range<IntegerWrapper>, result: IntegerWrapper) -> Error {
    Error::ArgumentOutOfDomain(range.start, range.end, result)
}

#[cold]
#[inline]
#[must_use]
pub const fn no_such_file_or_directory() -> Error {
    Error::NoSuchFileOrDirectory
}

#[cold]
#[inline]
#[must_use]
pub fn operation_not_permitted(reason: &str) -> Error {
    Error::OperationNotPermitted(reason.to_string().into())
}

#[cold]
#[inline]
#[must_use]
pub fn initialization_failed(reason: &str) -> Error {
    Error::InitializationFailed(reason.to_string().into())
}

#[cold]
#[inline]
#[must_use]
pub fn layer_not_present(layer: &str) -> Error {
    Error::LayerNotPresent(layer.to_string().into())
}

#[cold]
#[inline]
#[must_use]
pub const fn unknown() -> Error {
    Error::Unknown
}

#[cfg(feature = "sleipnir")]
#[allow(clippy::from_over_into)]
impl Into<Error> for sleipnir::Error {
    fn into(self) -> Error {
        match self {
            Self::Cancelled => operation_cancelled(),
            other => state_not_recoverable(&format(format_args!("{other}"))),
        }
    }
}

unsafe impl Send for Error {}
unsafe impl Sync for Error {}

impl Unpin for Error {}

impl Display for IntegerWrapper {
    #[inline]
    fn fmt(&self, f: &mut Formatter) -> core::fmt::Result {
        match self {
            Self::I8(value) => write!(f, "{value}"),
            Self::U8(value) => write!(f, "{value}"),
            Self::I16(value) => write!(f, "{value}"),
            Self::U16(value) => write!(f, "{value}"),
            Self::I32(value) => write!(f, "{value}"),
            Self::U32(value) => write!(f, "{value}"),
            Self::I64(value) => write!(f, "{value}"),
            Self::U64(value) => write!(f, "{value}"),
            Self::Isize(value) => write!(f, "{value}"),
            Self::Usize(value) => write!(f, "{value}"),
            Self::NonZeroIsize(value) => write!(f, "{value}"),
            Self::NonZeroUsize(value) => write!(f, "{value}"),
        }
    }
}

impl From<i8> for IntegerWrapper {
    #[inline]
    fn from(value: i8) -> Self {
        Self::I8(value)
    }
}

impl From<u8> for IntegerWrapper {
    #[inline]
    fn from(value: u8) -> Self {
        Self::U8(value)
    }
}

impl From<i16> for IntegerWrapper {
    #[inline]
    fn from(value: i16) -> Self {
        Self::I16(value)
    }
}

impl From<u16> for IntegerWrapper {
    #[inline]
    fn from(value: u16) -> Self {
        Self::U16(value)
    }
}

impl From<i32> for IntegerWrapper {
    #[inline]
    fn from(value: i32) -> Self {
        Self::I32(value)
    }
}

impl From<u32> for IntegerWrapper {
    #[inline]
    fn from(value: u32) -> Self {
        Self::U32(value)
    }
}

impl From<i64> for IntegerWrapper {
    #[inline]
    fn from(value: i64) -> Self {
        Self::I64(value)
    }
}

impl From<u64> for IntegerWrapper {
    #[inline]
    fn from(value: u64) -> Self {
        Self::U64(value)
    }
}

impl From<isize> for IntegerWrapper {
    #[inline]
    fn from(value: isize) -> Self {
        Self::Isize(value)
    }
}

impl From<usize> for IntegerWrapper {
    #[inline]
    fn from(value: usize) -> Self {
        Self::Usize(value)
    }
}

impl From<NonZeroIsize> for IntegerWrapper {
    #[inline]
    fn from(value: NonZeroIsize) -> Self {
        Self::NonZeroIsize(value)
    }
}

impl From<NonZeroUsize> for IntegerWrapper {
    #[inline]
    fn from(value: NonZeroUsize) -> Self {
        Self::NonZeroUsize(value)
    }
}
